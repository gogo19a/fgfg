import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title:'magnet',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const WebPage() ,
    );
  }
}
class WebPage extends StatefulWidget {
  const WebPage({Key? key}) : super(key: key);

  @override
  State<WebPage> createState() => _WebPageState();
}



WebViewController? controllerGlobal;

Future<bool> _exitApp(BuildContext context) async {
  if (await controllerGlobal!.canGoBack()) {
    controllerGlobal!.goBack();
    return Future.value(true);
  } else {
    return Future.value(false);
  }
}


class _WebPageState extends State<WebPage> {

  final Completer<WebViewController> _controller =
  Completer<WebViewController>();


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (Platform.isAndroid) WebView.platform = AndroidWebView();
  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop:()=> _exitApp(context),
      child: Scaffold(
        body: SafeArea(child: Expanded(child:WebView(
          initialUrl: "https://magnetcup.net",
            allowsInlineMediaPlayback: true,
            javascriptMode:JavascriptMode.unrestricted,
            onWebViewCreated: (WebViewController webViewController) {
              _controller.complete(webViewController);
              NavigationControls(_controller.future);
            }
        ) ,)),
      ),
    );
  }
}


class NavigationControls extends StatelessWidget {
  const NavigationControls(this._webViewControllerFuture)
      : assert(_webViewControllerFuture != null);

  final Future<WebViewController> _webViewControllerFuture;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<WebViewController>(
      future: _webViewControllerFuture,
      builder:
          (BuildContext context, AsyncSnapshot<WebViewController> snapshot) {
        if(snapshot.hasData){
          final bool webViewReady =
              snapshot.connectionState == ConnectionState.done;
          final WebViewController? controller = snapshot.data;
          controllerGlobal = controller;
        }

        return const Text("");
      },
    );
  }
}